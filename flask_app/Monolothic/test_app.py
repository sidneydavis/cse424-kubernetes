#!/bin/usr/python3

import os
import unittest
from app import app
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
import json

class AppTests(unittest.TestCase):
    def setUp(self):
        app.config["UPLOAD_FOLDER"] = "./uploads"
        self.app = app.test_client()
    def tearDown(self):
        pass

    def upload(self, upload_file):
        f = open(upload_file, "rb")
        return self.app.post(
            '/upload',
            data=dict(file=f),
            follow_redirects=True
        )

    def download(self, download_file):
        return self.app.get(
            '/download/'+str(download_file),
            follow_redirects=True
        )
###############
#### tests ####
###############

    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(200, response.status_code)

    def test_upload_page(self):
        response = self.app.get('/upload', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_upload(self):
        files = ["text.txt"]
        response = self.upload("text.txt")
        self.assertEqual(response.status_code, 200)

    def test_list_page(self):
        response = self.app.get('/list', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_download_page(self):
        response = self.app.get('/download', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_download(self):
        response = self.download("text.txt")
        self.assertEqual(response.status_code, 200)

    def test_aboutUs_page(self):
        response = self.app.get('/aboutUs', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_contactUs_page(self):
        response = self.app.get('/contactUs', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()