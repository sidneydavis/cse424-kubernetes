#!/bin/bash

#uncomment the next THREE lines to text on minikube
#minikube start --vm-driver=virtualbox
#minikube addons enable ingress
#eval $(minikube docker-env)


#build the flasktestapp locally
docker build --tag flasktestapp .

#add the flask app and service
kubectl create -f pod.yml
kubectl apply -f service.yml

#create the nginx ingress
kubectl apply -f ingress.yml

#create the nfs
kubectl apply -f pv.yaml
kubectl apply -f pvc.yaml

#create the deployment
kubectl apply -f deployment.yml




echo "\t\t\tUse the command:\n\n\t\t\tkubectl get ingress flask-ingress\n\n\t\t\tto get ip address"