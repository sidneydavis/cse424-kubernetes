#!/bin/bash
#start minikube
#macOS
minikube start --vm-driver=virtualbox
#Not macOS
    #minikube start

#Ingress handles all requests to the web app
minikube addons enable ingress

#Download MongoDB and MongoDB Express
#echo "Downloading MongoDB containers.."
#docker pull mongo
#docker pull mongo-express
#echo "MongoDB downloaded"

#Set docker env
##Required to deploy local docker images
eval $(minikube docker-env)

#Build image
echo "Removing Docker Image..."
#docker rm flasktestapp
echo "Building Docker Image..."
sh build.sh &
#docker run --name flasktestapp -p 5001:5001 flasktestapp


#Run in minikube
#creates port for outside of cluster communication
#imagepullpolic=Never ****Required for local images

echo "Running Docker Image on KCluster"
kubectl apply -f service.yml
kubectl apply -f pod.yml
kubectl apply -f ingress.yml
kubectl create -f deployment.yml

#kubectl run flasktestapp --image=flasktestapp --image-pull-policy=Never --port=8080

#poxysettings preventing kubectl from directly reaching it
#export no_proxy=$no_proxy,$(minikube ip)

#Check that it's running
kubectl get pods

#Run application on cluster

#minikube service flasktestapp --url

#kubectl cluster-info
#kubectl describe services

#List all pods
#kubectl get po -n kube-system

#go to the service (web app)
#minikube service flasktestapp
echo "The external ip address is:"
minikube ip
echo "On port 30006"
