import os
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
app = Flask(__name__)

app.config["UPLOAD_FOLDER"] = "./uploads"
@app.route('/')
def hello():
    return render_template("/index.html")

        
@app.route("/list", methods=['GET','POST'])
def list():
    uploadedFiles =[]
    for files in os.listdir("./uploads/"):
        if files != ".DS_Store" and files != ".iAmHiding":
            uploadedFiles.append(files)
    return render_template("/list.html", content = uploadedFiles)


if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5005, debug = True)
