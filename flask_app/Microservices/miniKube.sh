#!/bin/bash
minikube start --vm-driver=virtualbox
minikube addons enable ingress
eval $(minikube docker-env)

#Build required local docker images
docker build --tag flsk flaskapp/
docker build --tag aboutus aboutUs/
docker build --tag download download/
docker build --tag upload upload/
docker build --tag list list/




#add the flask app and servie (homepage)
kubectl create -f flaskapp/flaskapp.yml
kubectl apply -f flaskapp/flask-service.yml

#add the download app and service (download page)
kubectl apply -f download/download-pod.yml
kubectl apply -f download/download-service.yml

#add the aboutUs app and service (aboutUs page)
kubectl apply -f aboutUs/aboutus-service.yml
kubectl apply -f aboutUs/aboutus-pod.yml

#add the upload app and service (upload page)
kubectl apply -f upload/upload-pod.yml
kubectl apply -f upload/upload-service.yml

#create the nginx ingress
kubectl apply -f ingress/ingress.yml

#create the persistent volumes
kubectl apply -f pv.yaml
kubectl apply -f pvc.yaml

#create the deployment
kubectl create -f deployment.yml

kubectl get pods
kubectl get services
kubectl get ingress flask-ingress

<<<<<<< HEAD
echo "Use the command 'kubectl get ingress flask-ingress' to get ip address"
=======
echo "\t\t\tUse the command:\n\n\t\t\tkubectl get ingress flask-ingress\n\n\t\t\tto get ip address"
>>>>>>> cluster-final
