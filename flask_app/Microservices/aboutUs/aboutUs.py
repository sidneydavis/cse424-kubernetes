import os
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
app = Flask(__name__)

app.config["UPLOAD_FOLDER"] = "./uploads"

@app.route("/aboutUs")
def aboutUs():
    return render_template("/aboutUs.html")
        
if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5004, debug = True)
