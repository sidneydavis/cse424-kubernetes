import os
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
app = Flask(__name__)

app.config["UPLOAD_FOLDER"] = "./uploads"
@app.route('/')
def hello():
    return render_template("/index.html")
    
@app.route("/upload", methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        if request.files:
            file = request.files["file"]
            if file.filename == "":
                return render_template("upload.html")
            file.save(os.path.join(app.config["UPLOAD_FOLDER"], file.filename))
            return render_template("successupload.html")
    else:
        return render_template("upload.html")
        
if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5003, debug = True)
