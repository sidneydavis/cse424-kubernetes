import os
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
app = Flask(__name__)

app.config["UPLOAD_FOLDER"] = "./uploads"

@app.route("/download/")
def download():
    uploadedFiles = [f for f in os.listdir("./uploads/")]
    return render_template("download.html", content=uploadedFiles)

@app.route("/download/<path:filename>", methods= ['GET', 'POST'])
def downloadThis(filename):
    return send_from_directory(directory = './uploads/', filename = filename, as_attachment=True)
        
if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5002, debug = True)
