import os, sys
from flask import Flask, flash, redirect, url_for, render_template, request, send_from_directory
app = Flask(__name__)




def fib(n):
    #print(n)
    if n == 1:
        return [1]
    if n == 2:
        return [1, 1]
    fibs = [1, 1]
    for i in range(2, n):
        fibs.append(fibs[-1] + fibs[-2])
    
    return fibs

@app.route('/')
def __main__():
    return render_template("/index.html")

@app.route("/fib", methods = ['GET', 'POST'])
def main():
	n = request.args.get('n')
	n = int(n)
	m = 100000
	for i in range (0, n):
		val = fib(m)
		#print(i)
	return render_template("/fibview.html", val = val, m=m)



        
if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 6002, debug = True)
