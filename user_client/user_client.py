#!/usr/bin/python3
# ***** Sample Input ********
#  python3 file_service.py -u -n nothing.txt
# *************************

import argparse
import requests
import os
import sys
from user import User
import sshtunnel
import signal

def signal_handler(signal, frame):
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def arg_parser():
    parser = argparse.ArgumentParser()

    # CLI Arguments
    parser.add_argument('-u', '--upload', nargs='+', help='This option is used if you want to upload one or more files.'
                                                        + '\n Use [-u file1 file2 file3] if you want to upload more than one file.')
    parser.add_argument('-d', '--download', nargs='+', help='This option is used if you want to download one or more files.'
                                                        + '\n Use [-d file1 file2 file3] if you want to download more than one file.')
    parser.add_argument('-l', '--list', action='store_true', help='list all uploaded files')
    parser.add_argument('-p', '--pi', help='If you are want to SSH into the pi cluster, include a path to your SSH key under ~/.ssh/')
    parser.add_argument('-m', '--minikube', help="Enter the minikube cluster ip and port with the format: IP:PORT")
    parser.add_argument( '--dcl', action='store_true', help="docker compose local")
    return parser.parse_args()

def ask_user_input():
    print("Please enter a command to use the file server. You can choose from the following:")
    print("\t -h, --help            show this help message and exit")
    print("\t -u file [file1 file2 ...], --upload file [file1 file2 ...]")
    print("\t\tThis option is used if you want to upload one or more files."
            + "Use [-u file1 file2 file3] if you want to upload more than one file.")

    print("\t -d file [file1 file2 ...], --download file [file1 file2 ...]")
    print("\t\tThis option is used if you want to upload one or more files."
            + "Use [-d file1 file2 file3] if you want to upload more than one file.")

    print("\t -l, --list")
    print("\t\tThis option is used if you want to upload one or more files."
            + "Use [-u file1 file2 file3] if you want to upload more than one file.")

    print("\t -q, --quit, ")
    print("\t\tType -q/--quit to exit the program.")

def parse_input(user_input, user_session):
    split = user_input.split(" ")
    if split[0] == "-u" or split[0] == "--upload" :
        upload_files = split[1:]
        print(upload_files)
        user_session.postUploadList(upload_files)
    elif split[0] == "-d" or split[0] == "--download":
        download_files = split[1:]
        print(download_files)
        user_session.getDownloadList(download_files)
    elif split[0] == "-l" or split[0] == "--list":
        user_session.getList()
    elif split[0] == "-h" or split[0] == "--help":
        ask_user_input()
    elif split[0] == "-q" or split[0] == "--quit":
        os._exit(0)
    else:
        print("Incorrect input format. Please try again.")

def main():
    args = arg_parser()
    user_session = User(args)
    ask_user_input()
    user_input = input()
    parse_input(user_input, user_session)
    while True:
        print("Please enter a command:")
        user_input = input()
        parse_input(user_input, user_session)

if __name__ == '__main__':
    main()
