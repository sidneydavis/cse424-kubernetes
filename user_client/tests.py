#!/usr/bin/python3

import requests
import unittest
from unittest import mock
from unittest import TestCase
import responses
import argparse
from user import User
import sys
import os

class UserTest(TestCase):
    def __init__(self, methodName="runTest", args=None):
        parser = argparse.ArgumentParser()
        parser.add_argument('-m', '--minikube', action='store_true')
        parser.add_argument('-d', '--dcl', action='store_true')
        args = parser.parse_args()
        super().__init__(methodName)
        self.args = args
        self.args.minikube = "192.168.99.115:30006"
        self.user = User(args)

    def test_upload_invalid_file(self):
        test_file = ["testing"]
        self.user.postUploadList(test_file)
        self.assertEqual(None, self.user.response)

    def test_upload_text_file(self):
        test_files = ["bin/text.txt"]
        self.user.postUploadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_upload_multiple_files(self):
        test_files = ["bin/text.txt", "bin/cat.jpeg", "bin/data.csv"]
        self.user.postUploadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_upload_jpeg_file(self):
        test_files = ["bin/cat.jpeg"]
        self.user.postUploadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_upload_csv(self):
        test_files = ["bin/data.csv"]
        self.user.postUploadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_download_text_file(self):
        test_files = ["text.txt"]
        self.user.getDownloadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_download_invalid_file(self):
        test_files = ["invalid"]
        self.user.postUploadList(test_files)
        self.assertEqual(None, self.user.response)

    def test_download_multiple_files(self):
        test_files = ["text.txt", "cat.jpeg", "data.csv"]
        self.user.postUploadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_download_jpeg_file(self):
        test_files = ["cat.jpeg"]
        self.user.getDownloadList(test_files)
        self.assertEqual(200, self.user.response)

    def test_download_csv(self):
        test_files = ["data.csv"]
        self.user.getDownloadList(test_files)
        self.assertEqual(200, self.user.response)

if __name__ == '__main__':
    unittest.main()
