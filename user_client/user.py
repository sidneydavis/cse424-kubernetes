import requests
import paramiko
import shutil
import forward
import threading

PI_IP = "192.206.187.104"
PI_CLUSTER_IP = "192.168.1.240"
PI_CLUSTER_PORT = 5001
PI_PORT = 2222
LOCAL_PORT = 5555

class User():
    def __init__(self, args=None):
        self.args = args
        self.upload_list = None
        self.download_list = None
        self.ssh_key = None
        self.url = None
        self.transport = None
        self.response = None
        self._handle_args()

    def _handle_args(self):
        if self.args.minikube:
            self.url = "http://" + self.args.minikube
        elif self.args.pi:
            print(self.args.pi)
            self.ssh_key = self.args.pi
            self._connect_to_pi_cluster()
            self._start_tunnel_thread()
            self.url = "http://" + "localhost:" + str(LOCAL_PORT)
            print(self.url)
        elif self.args.dcl:
            self.url = "http://0.0.0.0"
            print("Docker compose local woo", self.url)
        '''
        if self.args.upload:
            self.upload_list = self.args.upload
            self.postUploadList(self.upload_list)
        if self.args.download:
            self.download_list = self.args.download
        '''

    def _start_tunnel_thread(self):
        thread = threading.Thread(target=self._forward_tunnel)
        thread.start()

    def _forward_tunnel(self):
        try:
            pk = paramiko.RSAKey.from_private_key_file(self.ssh_key)
            transport = paramiko.Transport((PI_IP,PI_PORT))
            transport.connect(username="pi",
                                pkey=pk)
            forward.forward_tunnel(LOCAL_PORT, PI_CLUSTER_IP, PI_CLUSTER_PORT, transport)
        except Exception as ex:
            print("Exception occurred.", ex)

    def _connect_to_pi_cluster(self):
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(PI_IP, port=2222, username="pi", key_filename=self.ssh_key)
        except Exception as e:
            print("Exception found:", e)
        else:
            # You can check if it is logged in on the master pi by cat /var/log/auth.log
            print("Successfully connected to the Pi cluster.")

    def postUploadList(self, upload_files):
        json = {}
        for f in upload_files:
            if not self.args.dcl:
                try:
                    json['file'] = open(f, 'rb')
                    response = requests.post(self.url + "/upload", files=json)
                    response.raise_for_status()
                    self.response = response.status_code
                    print("File '{0}' uploaded successfully.".format(f))
                except FileNotFoundError:
                    print("File {0} not found. Check the file name.".format(f))
                except Exception as e:
                    print("Exception found: ", e)
            else:
                try:
                    json['file'] = open(f, 'rb')
                    print(self.url + "/5003")
                    response = requests.post(self.url + ":5003/upload", files=json)
                    response.raise_for_status()
                    self.response = response.status_code
                    #response = requests.post(self.url + "/5003", files=json)
                    print("File '{0}' uploaded successfully.".format(f))
                except FileNotFoundError:
                    print("File {0} not found. Check the file name.".format(f))
                except Exception as e:
                    print("Exception found: ", e)

    def getDownloadList(self, download_files):
        print(download_files)
        for f in download_files:
            try:
                if not self.args.dcl:
                    # https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests
                    with requests.get(self.url+ '/download/' + f, stream=True) as response:
                        response.raise_for_status()
                        self.response = response.status_code
                        with open(f, 'wb') as download_file:
                            shutil.copyfileobj(response.raw, download_file) # supports up to ~40MB/s
                            '''
                            # only supports ~2-3MB/s
                            for chunk in response.iter_content(chunk_size=8192):
                                if chunk:
                                    f.write(chunk)
                                print("after copy")
                            '''
                        print("Finished downloading file {0}".format(f))
                else:
                    with requests.get(self.url+ ':5002/download/' + f, stream=True) as response:
                        response.raise_for_status()
                        self.response = response.status_code
                        with open(f, 'wb') as download_file:
                            shutil.copyfileobj(response.raw, download_file)
                        print("Finished downloading file {0}".format(f))
            except Exception as e:
                print("Exception has occurred:", e)


    def getList(self):
        try:
            if not self.args.dcl:
                with requests.get(self.url+ '/list', stream=True) as response:
                    response.raise_for_status()
                    self.response = response.status_code
                    print("\n", response.text)
            else:
                with requests.get(self.url+ ':5005/list', stream=True) as response:
                    response.raise_for_status()
                    self.response = response.status_code
                    print("\n", response.text)
        except Exception as e:
            print("Exception has occurred:", e)
